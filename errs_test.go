package errs

import (
	"fmt"
	"testing"
)

func TestRaiseErr(t *testing.T) {
	err := Raise(ErrError, "Test error")
	fmt.Println(err.ErrorF())
	fmt.Printf("%#v\n", err)
}
