# Пакет расширенных ошибок

Предоставляет тип Err{} реализующий интерфейс error

Дополнительно производит вывод сообщения в StdErr с форматом заданным переменной `errs.StrFmt`

Для прекращения вывода требуется изменить переменную `errs.Silent = true`

Рекомендуемое использование:

```go

const MyError errs.ErrCode = "MyError"

func sub() *errs.Err {
    return errs.Raise(MyError,"Error description",map[string]interface{}{"Ext":"This data will be saved to database error log as JSON string"})
}

func main(){
    err:=sub()
    errs.Up(err,map[string]interface{}{"Ext":"This will be saved to database with previos error data"})
}
```
