package errs

import (
	"fmt"
	"os"
	"runtime"

	"gitlab.com/leolab/go/replacer"
)

type ErrCode string

var Silent = false
var StrFmt = "%date% %time% [%ecode%] %error% at %file%:%line% %func%"

const (
	ErrNoError ErrCode = "ErrNoError"

	//Global errors
	ErrError          ErrCode = "ErrError"
	ErrNotImplemented ErrCode = "ErrNotImplemented"
)

type __src struct {
	File string
	Line int
	Func string
}

type Err struct {
	Code ErrCode
	Msg  string
	Data interface{}
	Src  __src
	Prev *Err
}

func (e *Err) Error() string {
	if e == nil {
		panic("NIL Error")
	}
	return e.Msg
}

func (e *Err) ErrorF() string {
	if e == nil {
		panic("NIL Error")
	}
	return e.__getString()
}

func Raise(eCode ErrCode, msg string, data ...interface{}) *Err {
	e := &Err{Code: eCode, Msg: msg, Data: data, Src: __getSrc(2), Prev: nil}
	__logError(e)
	return e
}

func Up(err *Err, data ...interface{}) *Err {
	e := &Err{
		Code: err.Code,
		Msg:  err.Msg,
		Data: data,
		Src:  __getSrc(2),
		Prev: err,
	}
	return e
}

func __getSrc(lvl int) (s __src) {
	pc, file, line, ok := runtime.Caller(lvl)
	if !ok {
		return __src{File: "undefined", Line: 0, Func: "undefined()"}
	}
	f := runtime.CallersFrames([]uintptr{pc})
	ff, _ := f.Next()
	s = __src{
		File: file,
		Line: line,
		Func: ff.Function,
	}
	return s
}

func (e *Err) __getString() string {
	if e == nil {
		panic("Err is NIL")
	}
	r := replacer.NewReplacer()
	r.Add("ecode", string(e.Code))
	r.Add("file", e.Src.File)
	r.Add("line", fmt.Sprintf("%v", e.Src.Line))
	r.Add("func", e.Src.Func)
	r.Add("error", e.Msg)
	return r.ReplaceC(StrFmt)
}

func __logError(e *Err) {
	if Silent {
		return
	}
	fmt.Fprintln(os.Stderr, e.__getString())
}
